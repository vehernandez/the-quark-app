[![pipeline status](https://gitlab.com/vehernandez/the-quark-app/badges/master/pipeline.svg)](https://gitlab.com/vehernandez/the-quark-app/commits/master)[![coverage report](https://gitlab.com/vehernandez/the-quark-app/badges/master/coverage.svg)](https://gitlab.com/vehernandez/the-quark-app/commits/master)[![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/42/badge)](https://bestpractices.coreinfrastructure.org/projects/42)

# The Quark App

**The Quark App** is a delightfully simple application allowing to quickly highlight main capabilities in GitLab through a few examples, walking through the [GitLab Flow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html).

## What Does It Do?

From an [Issue](https://docs.gitlab.com/ee/user/project/issues/#use-cases) to a fully-deployed **Dockerized**  application running in a **Kubernetes** cluster [Environment](https://docs.gitlab.com/ce/ci/environments.html), *The Quark App* shows the business value of a Cloud-Native, single platform enabling the entire DevOps lifecycle, from Idea to Production.

## How Does One Walk Through The Flow?

A **Business User** - a Product Owner, Project Manager, Team Lead, or any other type of requestor - creates an **Issue** expressing the business value of the request, whether a *User Story*, *Defect*, *IT Task*, or other item for the **Development Teams**.

A Team Member - a **Developer** - picks up the Issue, and creates a **Merge Request** directly from it.  The automated worfklow creates a corresponding **Branch**, and on first commit, kicks off a **Pipeline**.